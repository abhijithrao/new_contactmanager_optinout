package robinson.batch.helper

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 5/1/16
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */

uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses robinson.batch.helper.RobinsonImportedRecordDTO
uses org.apache.commons.lang.StringUtils
uses java.text.SimpleDateFormat
uses java.util.Date

class RobinsonHelper {


  private static final var LOGGER: Logger = LoggerFactory.getLogger(RobinsonHelper)
  public var robinsonRecordDTO: RobinsonImportedRecordDTO
  var gwContact: entity.ABContact
  var areAllCriteriaMatchFound: boolean = false
  var isPostCodeMatchFound: boolean = false
  var isHouseNumberFound: boolean = false
  var isLastNameMatchFound: boolean = false
  var isFirstNameMatchFound: boolean = false
  var isCityMatchFound: boolean = false
  var isStreetMatchFound: boolean = false
  var isCountryMatchFound: boolean = false

  construct(contact: ABContact, record: RobinsonInboundRec_Ext) {

    robinsonRecordDTO = new RobinsonImportedRecordDTO(record)
    gwContact = contact
  }

   public function getRobinsonDTO(): RobinsonImportedRecordDTO {
    if (robinsonRecordDTO != null){
      return robinsonRecordDTO
    }
    return null
  }

  /**
   *  Validates number of true values and increments the count of matching criterias
   *  Sets the count of matching criterias
   */
  @Param("propName", "Property Name")
  @Returns("True if the property value identified by property name evaluates to true")
  function validateAndGetCount() {
    if (robinsonRecordDTO != null && gwContact != null){

      if (gwContact typeis ABPerson)  {

        isPostCodeMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress?.PostalCode, robinsonRecordDTO.PostalCode)
        isHouseNumberFound = findMatchesInAddress(gwContact?.PrimaryAddress, robinsonRecordDTO.HouseNumber)
        isLastNameMatchFound = findMatchesInAddress(gwContact?.LastName, robinsonRecordDTO.LastName)
        isFirstNameMatchFound = findMatchesInAddress(gwContact?.FirstName, robinsonRecordDTO.FirstName)
        isCityMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress?.City, robinsonRecordDTO.Locality)
        //isStreetMatchFound = findMatchesInAddress(StringUtils.deleteWhitespace(gwContact?.PrimaryAddress), robinsonRecordDTO.Street)
        isStreetMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress, robinsonRecordDTO.Street)
        isCountryMatchFound = findMatchesInAddress(gwContact?.PrimaryAddress?.Country, Country.TC_BE.Code)
      }
      LOGGER.info("\n isPostCodeMatchFound =" + isPostCodeMatchFound +
          "\tisHouseNumberFound = " + isHouseNumberFound +
          "\tisLastNameMatchFound =" + isLastNameMatchFound +
          "\tisFirstNameMatchFound =" + isFirstNameMatchFound +
          "\tisCityMatchFound =" + isCityMatchFound +
          "\tisStreetMatchFound =" + isStreetMatchFound +
          "\tisCountryMatchFound =" + isCountryMatchFound)
    }
  }

  function countOfCriteriaMatched(): int {
    validateAndGetCount()
    var count = 0

    if (isPostCodeMatchFound) count++
    if (isHouseNumberFound)  count++
    if (isLastNameMatchFound)  count++
    if (isFirstNameMatchFound) count++
    if (isCityMatchFound)  count++
    if (isStreetMatchFound)  count++
    if (isCountryMatchFound)  count++
    LOGGER.info(" TOTAL Matche Criterias Found : \t" + count)
    return count
  }

  function countOfCompulsoryCriteriaMatched(): int {
    validateAndGetCount()
    var count = 0

    if (isPostCodeMatchFound) count++
    if (isHouseNumberFound)  count++
    if (isLastNameMatchFound || isFirstNameMatchFound)  count++
    if (isCityMatchFound)  count++
    if (isStreetMatchFound)  count++
    if (isCountryMatchFound)  count++
    return count
  }

  function findMatchesInAddress(primaryAddress: String, portionToMatch: String): boolean {
    if (primaryAddress != null && portionToMatch != null){

      return primaryAddress.containsIgnoreCase(portionToMatch)
    }
    return false
  }

  public static function getDateFromddMMyyFormat(dateString: String): Date {
    var df = new SimpleDateFormat("dd/MM/yy")
    var convertCurrentDate = dateString
    var date = new java.util.Date()
    date = df.parse(convertCurrentDate)
    return date
  }
}