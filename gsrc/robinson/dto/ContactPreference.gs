package robinson.dto

uses java.util.Date
/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/20/16
 * Time: 4:58 PM
 * To change this template use File | Settings | File Templates.
 */
class ContactPreference {

  var preference                  : Preference  as Preference
  var preferenceUpdatedDate       : Date        as PreferenceUpdatedDate
  var source                      : String      as SourceOfUpdate

}