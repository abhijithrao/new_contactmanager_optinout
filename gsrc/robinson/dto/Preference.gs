package robinson.dto
/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/20/16
 * Time: 5:00 PM
 * To change this template use File | Settings | File Templates.
 */
 final enum Preference {

      DigitalContactPreference("DigitalContactPreference"),
      PhoneContactPreference("PhoneContactPreference"),
      PaperMailContactPreference("PaperMailContactPreference")

      private var PreferenceValue : String
      private construct(value1 :String) {
        this.PreferenceValue = value1
      }

      function getEnrichmentValue():String {
        return PreferenceValue
      }


    }