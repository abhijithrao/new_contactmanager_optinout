package util.common
/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 4/21/16
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
    uses java.lang.*
    uses java.text.SimpleDateFormat
    uses java.util.ArrayList
    uses java.util.List
    uses util.IntegrationPropertiesUtil

public interface CommonConstants  {

    public static final var INFLIGHT : String = "inflight"
    public static final var DELIVERED : String = "DELIVERED"
    public static final var FTP_PROCESSED : String = "FTP_PROCESSED"
    public static final var FTP_FAILED : String = "FTP_FAILED"
    public static final var SUCCESS : String = "SUCCESSS"
    public static final var FAILED : String = "FAILED"
    public static final var ARCHIVED : String = "ARCHIVED"
    public static final var ARCHIEVE_FILE_SEPERATOR : String = "\\"
    public static final var ARCHIEVE_FILE_DOUBLESEPERATOR : String = "\\\\"
    public static final var ARCHIEVE_FOLDERNAME : String = "archive"
    public static final var METHOD_ENTER : String = "Entering Method: "
    public static final var METHOD_EXIT : String = "Exiting Method: "


    public static final var DATE_TIME_FORMAT : SimpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmm")
    public static final var ROBINSONS_FTP_DIRECTORY_PATH : String = IntegrationPropertiesUtil.getProperty("ROBINSONS.ftp.directory")
    public static final var ROBINSONS_FTP_SERVER : String = IntegrationPropertiesUtil.getProperty("ROBINSONS.ftp.server")
    public static final var ROBINSONS_FTP_PORT : String = IntegrationPropertiesUtil.getProperty("ROBINSONS.ftp.port")
    public static final var ROBINSONS_FTP_USER : String = IntegrationPropertiesUtil.getProperty("ROBINSONS.ftp.user")
    public static final var ROBINSONS_FTP_PASSWORD : String = IntegrationPropertiesUtil.getProperty("ROBINSONS.ftp.password")
/*    public static final var GW_USERNAME : String = IntegrationPropertiesUtil.getProperty("gw.pc.username")
    public static final var GW_PASSWORD : String = IntegrationPropertiesUtil.getProperty("gw.pc.password")*/


}
